DROP TABLE IF EXISTS users;
CREATE TABLE measurements (
  id integer primary key autoincrement,
  temperature double precision,
  light double precision,
  proximity double precision,
  noise double precision
);
