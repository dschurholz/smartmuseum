#include <SoftwareSerial.h>
#include <SFE_BMP180.h>

#define NOISE 0
#define RX 10
#define TX 11

String AP = "DSHS";
String PASS = "deutschland888";
String HOST = "192.168.43.196";
String PORT = "8888";
int countTrueCommand;
int countTimeCommand; 
int connNum = 0;
boolean found = false; 

int valNoiseSensor = 1;
int valProximity = 1;
int valLight = 1;
int noiseSensorPin = 8;
int ledPin = 13;
double valTemp = 0.0;
double tempThreshold = 27.0;

SFE_BMP180 tempSens;
SoftwareSerial esp8266(RX,TX); 
 
  
void setup() {
  Serial.begin(9600);

  // UDP Connection
//  esp8266.begin(115200);
  Serial.println("Starting Connection....");
//  sendCommand("AT",5,"OK");
//  sendCommand("AT+CWMODE=1",5,"OK");
//  sendCommand("AT+CWJAP=\""+ AP +"\",\""+ PASS +"\"",20,"OK");
  
  //Sensors setting
  pinMode(noiseSensorPin, INPUT);
  pinMode(ledPin, OUTPUT);

  if (tempSens.begin())
    Serial.println("BMP180 init success");
  else
  {
    // Oops, something went wrong, this is usually a connection problem,
    // see the comments at the top of this sketch for the proper connections.

    Serial.println("BMP180 init fail\n\n");
    while(1); // Pause forever.
  }
}
void loop() {
  char status;

  valNoiseSensor = digitalRead(noiseSensorPin); String valNoiseSensorStr = String(valNoiseSensor);
  String valProximitySensorSDAtr = String(0);
  String valProximitySensorSLCStr = String(0);
  String valTempStr = "NaN";

  status = tempSens.startTemperature();
  if (status != 0)
  {
    // Wait for the measurement to complete:
    delay(status);

    // Retrieve the completed temperature measurement:
    // Note that the measurement is stored in the variable T.
    // Function returns 1 if successful, 0 if failure.

    status = tempSens.getTemperature(valTemp);
    if (status != 0)
    {
      valTempStr = String(valTemp);

      if (valTemp > tempThreshold) {
        digitalWrite(ledPin, HIGH);
      } else {
        digitalWrite(ledPin, LOW);
      }
      // Start a tempSens measurement:
      // The parameter is the oversampling setting, from 0 to 3 (highest res, longest wait).
      // If request is successful, the number of ms to wait is returned.
      // If request is unsuccessful, 0 is returned.
    } else Serial.println("error retrieving temperature measurement\n");
  } else Serial.println("error starting temperature measurement\n");
  
  String datagram = valNoiseSensorStr + "," + valProximitySensorSDAtr + "," + valProximitySensorSLCStr + "," + valTempStr;
//  sendDatagram(datagram);
  Serial.println(datagram);
}

int getSensorData(){
  return random(1000);
}

void sendDatagram(String datagram) {
  String datagramLen = String(datagram.length());
  if (!found) {
    sendCommand("AT+CIPMUX=1",1,"OK");
  }
  sendCommand("AT+CIPSTART="+String(connNum)+",\"UDP\",\""+ HOST +"\","+ PORT,1,"OK");
  sendCommand("AT+CIPSEND="+String(connNum)+"," + datagramLen,1,">");
  Serial.println("> " + datagram);esp8266.println(datagram);countTrueCommand++;
//  sendCommand("AT+CIPCLOSE="+String(connNum)+"",1,"OK");
}

void sendCommand(String command, int maxTime, char readReplay[]) {
  Serial.print(countTrueCommand);
  Serial.print(". at command => ");
  Serial.print(command);
  Serial.print(" ");
  while(countTimeCommand < (maxTime*1))
  {
    esp8266.println(command);//at+cipsend
    if(esp8266.find(readReplay))//ok
    {
      found = true;
      break;
    }
  
    countTimeCommand++;
  }
  
  if(found == true)
  {
    Serial.println("Successful");
    countTrueCommand++;
    countTimeCommand = 0;
  }
  
  if(found == false)
  {
    Serial.println("Fail");
    countTrueCommand = 0;
    countTimeCommand = 0;
  }
  
  found = false;
 }
