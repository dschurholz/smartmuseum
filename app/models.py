from app import db
import datetime


class Measurement(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    temperature = db.Column(db.Float())
    proximity = db.Column(db.Float())
    light = db.Column(db.Float())
    noise = db.Column(db.Float())
    created_date = db.Column(db.DateTime, default=datetime.datetime.now)

    def __init__(self, temperature, proximity, light, noise):
        self.temperature = temperature
        self.proximity = proximity
        self.light = light
        self.noise = noise

    def __repr__(self):
        return '<User %r>' % self.id

    def serialize(self):
        return {
            'temperature': self.temperature,
            'proximity': self.proximity,
            'light': self.light,
            'noise': self.noise,
            'created_date': self.created_date
        }
