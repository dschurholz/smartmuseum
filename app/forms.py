from flask_wtf import FlaskForm
from wtforms import FloatField
from wtforms.validators import InputRequired

class MeasurementForm(FlaskForm):
    temperature = FloatField('Temperature', validators=[InputRequired()])
    light = FloatField('Light', validators=[InputRequired()])
    proximity = FloatField('Proximity', validators=[InputRequired()])
    noise = FloatField('Noise', validators=[InputRequired()])
