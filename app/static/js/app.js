/* Add your Application JavaScript */

function doPoll(){
    $.get('/get-last-measurement', function(data) {
        console.log(data);
        setTimeout(doPoll,1000);
        if (data["temperature"] <= 25) {
                $(".temp").addClass("bg-success");
                $(".temp").removeClass("bg-warning");
                $(".temp").removeClass("bg-danger");
        } else if (data["temperature"] <= 29) {
                $(".temp").removeClass("bg-success");
                $(".temp").addClass("bg-warning");
                $(".temp").removeClass("bg-danger");
        } else {
                $(".temp").removeClass("bg-success");
                $(".temp").removeClass("bg-warning");
                $(".temp").addClass("bg-danger");
        }
        $(".temp").html(Math.round(data["temperature"]) + " &#8451;");
        var light = Math.round(data["light"] * 0.9765625);
        if (light <= 400) {
                $(".light").addClass("bg-success");
                $(".light").removeClass("bg-warning");
                $(".light").removeClass("bg-danger");
        } else if (light <= 800) {
                $(".light").removeClass("bg-success");
                $(".light").addClass("bg-warning");
                $(".light").removeClass("bg-danger");
        } else {
                $(".light").removeClass("bg-success");
                $(".light").removeClass("bg-warning");
                $(".light").addClass("bg-danger");
        }
        $(".light").html(light + " Lux");
        if (!data["noise"]) {
            $(".noise").removeClass("bg-success");
            $(".noise").addClass("bg-danger");
            $(".noise").html("<i class=\"glyphicon glyphicon-volume-up\"></i>");
        } else {
            $(".noise").addClass("bg-success");
            $(".noise").removeClass("bg-danger");
            $(".noise").html("<i class=\"glyphicon glyphicon-volume-down\"></i>");
        }
        if (data["proximity"]) {
            $(".prox").removeClass("bg-success");
            $(".prox").addClass("bg-danger");
            $(".prox").html("<i class=\"glyphicon glyphicon glyphicon-hand-up\"></i>");
        } else {
            $(".prox").addClass("bg-success");
            $(".prox").removeClass("bg-danger");
            $(".prox").html("<i class=\"glyphicon glyphicon-unchecked\"></i>");
        }
    });
}

$(document).ready(function(){
    if ($(location).attr('pathname') == "/") {
        doPoll();
    }

    switch ($(location).attr('pathname')) {
        case "/measurements":
            $(".nav .navbar-nav > li").each(function(elem) {
                elem.removeClass("active");
            });
            $("#show-link").addClass("active");
            break;
        case "/add-measurement":
            $(".nav .navbar-nav > li").each(function(elem) {
                elem.removeClass("active");
            });
            $("#add-link").addClass("active");
            break;
        case "/about":
            $(".nav .navbar-nav > li").each(function(elem) {
                elem.removeClass("active");
            });
            $("#about-link").addClass("active");
            break;
        default:
            $(".nav .navbar-nav > li").each(function(elem) {
                elem.removeClass("active");
            });
            $("#home-link").addClass("active");
    }
});