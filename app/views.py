"""
Flask Documentation:     http://flask.pocoo.org/docs/
Jinja2 Documentation:    http://jinja.pocoo.org/2/documentation/
Werkzeug Documentation:  http://werkzeug.pocoo.org/documentation/
This file creates your application.
"""

from app import app, db
from flask import render_template, request, redirect, url_for, flash, jsonify
from .forms import MeasurementForm
from .models import Measurement
# import sqlite3

###
# Routing for your application.
###

@app.route('/')
def home():
    """Render website's home page."""
    return render_template('home.html')


@app.route('/about')
def about():
    """Render the website's about page."""
    return render_template('about.html', name="Smart Museums Inc.")

@app.route('/measurements')
def measurements():
    measurements = db.session.query(Measurement).order_by(Measurement.id.desc()).limit(500).all()

    return render_template('show_users.html', measurements=measurements)


@app.route('/get-last-measurement')
def get_last_measurement():
    measurement = db.session.query(Measurement).order_by(Measurement.id.desc()).first()

    return jsonify(measurement.serialize())

@app.route('/add-measurement', methods=['POST', 'GET'])
def add_measurement():
    measurement_form = MeasurementForm()

    if request.method == 'POST':
        if measurement_form.validate_on_submit():
            # Get validated data from form
            temperature = measurement_form.temperature.data
            proximity = measurement_form.proximity.data
            light = measurement_form.light.data
            noise = measurement_form.noise.data

            # save measurement to database
            measurement = Measurement(temperature, proximity, light, noise)
            db.session.add(measurement)
            db.session.commit()

            flash('Measurement successfully added')
            return redirect(url_for('measurements'))

    flash_errors(measurement_form)
    return render_template('add_user.html', form=measurement_form)

# Flash errors from the form if validation fails
def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (
                getattr(form, field).label.text,
                error
            ))

###
# The functions below should be applicable to all Flask apps.
###

@app.route('/<file_name>.txt')
def send_text_file(file_name):
    """Send your static text file."""
    file_dot_text = file_name + '.txt'
    return app.send_static_file(file_dot_text)


@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=600'
    return response


@app.errorhandler(404)
def page_not_found(error):
    """Custom 404 page."""
    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0",port="8080")
