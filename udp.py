from __future__ import print_function
import socket
import time
import random

from app import db
from app.models import Measurement


def start_udp(delay=10.0):
    UDP_IP = "192.168.43.196"
    UDP_PORT = 8888
    MESSAGE = "Starting connection in:" + str(UDP_IP) + ":" + str(UDP_PORT)

    print("UDP target IP:", UDP_IP)
    print("UDP target port:", UDP_PORT)
    print(MESSAGE)

    serverSocket = socket.socket(
        socket.AF_INET,  # Internet
        socket.SOCK_DGRAM  # UDP
    )
    serverSocket.bind((UDP_IP, UDP_PORT))
    while True:
        data, addr = serverSocket.recvfrom(1024)
        values = data.decode("utf-8").split(",")

        try:
            noise = float(values[0])
            prox = float(values[1])
            light = float(values[2])
            temp = float(values[3])
        except:
            noise = float(values[0])
            prox = float(values[1])
            light = float(values[2])
            temp = float(values[3])
            print("erroneusData: ", data)
            print(addr)
            continue

        print("received data: ", values)
        print(addr)

        # save measurement to database
        measurement = Measurement(temp, prox, light, noise)
        db.session.add(measurement)
        db.session.commit()


if __name__ == "__main__":
    start_udp()
